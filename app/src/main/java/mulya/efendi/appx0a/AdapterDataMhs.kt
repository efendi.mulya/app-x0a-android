package mulya.efendi.appx0a

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_mahasiswa.*

class AdapterDataMhs(val dataMhs : List<HashMap<String,String>>,
                     val mahasiswaActivity: MahasiswaActivity) :
    RecyclerView.Adapter<AdapterDataMhs.HolderDataMhs>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AdapterDataMhs.HolderDataMhs {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.row_mhs,parent,false)
        return HolderDataMhs(v)
    }

    override fun getItemCount(): Int {
        return dataMhs.size
    }

    override fun onBindViewHolder(holder: AdapterDataMhs.HolderDataMhs, position: Int) {
        val data = dataMhs.get(position)
        holder.txNim.setText(data.get("nim"))
        holder.txNama.setText(data.get("nama"))
        holder.txProdi.setText(data.get("nama_prodi"))
        holder.txAlamat.setText(data.get("alamat"))
        holder.txJenisKelamin.setText(data.get("jenis_kelamin"))

        if (position.rem(2) == 0) holder.cLayout.setBackgroundColor(
            Color.rgb(230,245,240))
        else holder.cLayout.setBackgroundColor(Color.rgb(255,255,245))

        holder.cLayout.setOnClickListener(View.OnClickListener {
            val pos = mahasiswaActivity.daftarProdi.indexOf(data.get("nama_prodi"))
            mahasiswaActivity.spinProdi.setSelection(pos)
            mahasiswaActivity.edNim.setText(data.get("nim"))
            mahasiswaActivity.edNamaMhs.setText(data.get("nama"))
            Picasso.get().load(data.get("url")).into(mahasiswaActivity.imUpload);
            mahasiswaActivity.edAlamatMhs.setText(data.get("alamat"))
            mahasiswaActivity.edJenisKelamin.setText(data.get("jenis_kelamin"))
        })

        if (!data.get("url").equals(""))
            Picasso.get().load(data.get("url")).into(holder.photo);
    }

    class HolderDataMhs(v : View) : RecyclerView.ViewHolder(v){
        val txNim = v.findViewById<TextView>(R.id.txNim)
        val txNama = v.findViewById<TextView>(R.id.txNama)
        val txProdi = v.findViewById<TextView>(R.id.txProdi)
        val photo = v.findViewById<ImageView>(R.id.imageView)
        val txAlamat = v.findViewById<TextView>(R.id.txAlamat)
        val txJenisKelamin = v.findViewById<TextView>(R.id.txJenisKelamin)
        val cLayout = v.findViewById<ConstraintLayout>(R.id.cLayout)
    }
}